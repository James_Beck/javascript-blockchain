const express = require('express')
const app = express()
const bodyParser = require('body-parser')

let port = 3000

process.argv.forEach(function(val,index,array) {

  port = array[2]

})

if (port == undefined) {
  port = 3000
}

let Block = require('./block')
let Blockchain = require('./blockchain')
let Transaction = require('./transaction')
let BlockchainNode = require('./blockchainNode')

let fetch = required('node-fetch')

let genesisBlock = new Block()
let blockchain = new Blockchain(genesisBlock)
let transactions = []
let nodes = []

app.use(bodyParser.json())

app.get('/resolve', function(request,response) {

  nodes.forEach(function(node) {

    fetch(node.url + '/blockchain')
    .then(function(response) {
      return response.json()
    })
    .then(function(blockchain) {
      console.log(blockchain)
    })
  })

})

app.post('/nodes/register', function(request,response) {

  let nodesList = request.body.urls
  nodesList.forEach(function(nodeDictionary) {
    let node = new BlockchainNode(nodeDictionary["url"])
    nodes.push(node)
  })

  response.json(nodes)
})

app.get('/nodes', function(request,response) {

  response.json(nodes)
})

app.get('/', function(request,response) {
  response.send("hello world")
})

app.get('/mine', function(request,response) {

  let block = blockchain.getNextBlock(transactions)
  blockchain.addBlock(block)
  transactions = []
  response.json(block)

})

app.post('/transactions', function(request, response) {

  let from = request.body.from
  let to = request.body.to
  let amount = request.body.amount

  let transaction = new Transaction(from,to,amount)
  transactions.push(transaction)

  response.json(transactions)
})

app.get('/blockchain', function(request,response) {

  let transaction = new Transaction('Mary','Jerry',100)

  let genesisBlock = new Block()
  let blockchain = new Blockchain(genesisBlock)

  let block = blockchain.getNextBlock([transaction])
  blockchain.addBlock(block)

  let anotherTransaction = new Transaction("Azam","Jerry",10)
  let block1 = blockchain.getNextBlock([anotherTransaction,transaction])
  blockchain.addBlock(block1)

  response.json(blockchain)

})

app.listen(port, function() {
  console.log("server has started")
})
